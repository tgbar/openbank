package com.backbase.transactions.controllers;

import com.backbase.transactions.model.BackbaseTransaction;
import com.backbase.transactions.model.TotalAmount;
import com.backbase.transactions.model.openbank.Transactions;
import com.backbase.transactions.parser.TransactionParser;
import com.backbase.transactions.services.TransactionsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TransactionsService.class, RestTemplate.class})
public class TransactionsControllersTest {
    @InjectMocks
    private TransactionsControllers controller;
    @Mock
    private TransactionsService transactionsService;
    private MockMvc mockMvc;
    private Transactions openBankTransactions;
    private List<BackbaseTransaction> fullTransactionsList;
    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setup() throws IOException {
        MockitoAnnotations.initMocks(this);
        mockMvc = standaloneSetup(controller).build();
        openBankTransactions = objectMapper.readValue(new File("src/main/resources/transactions/transactions.json"), Transactions.class);
        fullTransactionsList = TransactionParser.fromOpenbankToBackbase(openBankTransactions.getTransactions());
    }

    @Test
    public void whenGetAllTransactions_shouldReturnStatusOk() throws Exception {
        when(transactionsService.getAllTransactions()).thenReturn(fullTransactionsList);

        MvcResult mvcResult = mockMvc.perform(get("/transactions"))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals(mvcResult.getResponse().getContentAsString(), objectMapper.writeValueAsString(fullTransactionsList));
    }

    @Test
    public void whenGetTransactionByType_shouldReturnStatusOk() throws Exception {
        final String transactionType = "SANDBOX_TAN";
        List<BackbaseTransaction> filteredTransactions = Arrays.asList(objectMapper.readValue(new File("src/main/resources/transactions/sandbox_tan_types.json"), BackbaseTransaction[].class));

        when(transactionsService.getTransactionByType(transactionType)).thenReturn(filteredTransactions);
        MvcResult mvcResult = mockMvc.perform(get("/transaction").param("transactionType", transactionType))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals(mvcResult.getResponse().getContentAsString(), objectMapper.writeValueAsString(filteredTransactions));
    }

    @Test
    public void whenGetAmountByType_shouldGroupAndSumCorrecly() throws Exception {
        List<TotalAmount> amounts = Arrays.asList(objectMapper.readValue(new File("src/main/resources/transactions/groupByAmount.json"), TotalAmount[].class));

        when(transactionsService.getTotalAmountByType()).thenReturn(amounts);

        MvcResult mvcResult = mockMvc.perform(get("/transactions/amount"))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals(mvcResult.getResponse().getContentAsString(), objectMapper.writeValueAsString(amounts));
    }
}
