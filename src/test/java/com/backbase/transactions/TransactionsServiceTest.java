package com.backbase.transactions;

import com.backbase.transactions.model.BackbaseTransaction;
import com.backbase.transactions.model.TotalAmount;
import com.backbase.transactions.model.openbank.Transactions;
import com.backbase.transactions.parser.TransactionParser;
import com.backbase.transactions.services.TransactionsService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { RestTemplate.class })
public class TransactionsServiceTest {

    @Mock
    private RestTemplate restTemplate;
    @Mock
    private Environment environment;
    @InjectMocks
    private TransactionsService transactionsService;
    private ObjectMapper objectMapper = new ObjectMapper();
    private Transactions openBankTransactions;
    private List<BackbaseTransaction> fullTransactionsList;

    @Before
    public void setup() throws IOException {
        MockitoAnnotations.initMocks(this);
        openBankTransactions = objectMapper.readValue(new File("src/main/resources/transactions/transactions.json"), Transactions.class);
        fullTransactionsList = TransactionParser.fromOpenbankToBackbase(openBankTransactions.getTransactions());
    }

    @Test
    public void whenGetAllTransactions_returnParsedTransactions() throws JsonProcessingException {
        when(restTemplate.getForObject(any(String.class), eq(Transactions.class))).thenReturn(openBankTransactions);
        List<BackbaseTransaction> backbaseTransactions = transactionsService.getAllTransactions();

        assertEquals(objectMapper.writeValueAsString(backbaseTransactions), objectMapper.writeValueAsString(fullTransactionsList));
    }

    @Test
    public void whenGetTransactionsByType_returnFilteredTransactions() throws IOException {
        final String transactionType = "SANDBOX_TAN";
        List<BackbaseTransaction> filteredTransactions = Arrays.asList(objectMapper.readValue(new File("src/main/resources/transactions/sandbox_tan_types.json"), BackbaseTransaction[].class));
        when(restTemplate.getForObject(any(String.class), eq(Transactions.class))).thenReturn(openBankTransactions);
        List<BackbaseTransaction> backbaseTransactions = transactionsService.getTransactionByType(transactionType);

        assertEquals(objectMapper.writeValueAsString(backbaseTransactions), objectMapper.writeValueAsString(filteredTransactions));
    }

    @Test
    public void whenGroupAmountByType_returnSuccessfullyCalculatedSums() throws IOException {
        List<TotalAmount> amounts = Arrays.asList(objectMapper.readValue(new File("src/main/resources/transactions/groupByAmount.json"), TotalAmount[].class));
        when(restTemplate.getForObject(any(String.class), eq(Transactions.class))).thenReturn(openBankTransactions);
        List<TotalAmount> returnedAmounts = transactionsService.getTotalAmountByType();

        assertEquals(objectMapper.writeValueAsString(returnedAmounts), objectMapper.writeValueAsString(amounts));
    }
}
