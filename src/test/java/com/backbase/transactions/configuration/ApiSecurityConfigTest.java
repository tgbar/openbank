package com.backbase.transactions.configuration;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.*;

@ContextConfiguration
@WebAppConfiguration
public class ApiSecurityConfigTest {
    @InjectMocks
    private ApiSecurityConfig apiSecurityConfig;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenAppInitializes_userDetailsServiceShouldNotBeNull() throws Exception {
        UserDetailsService userDetailsService = apiSecurityConfig.userDetailsService();
        assertEquals(userDetailsService instanceof InMemoryUserDetailsManager, true);
    }
}
