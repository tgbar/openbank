package com.backbase.transactions.parser;

import com.backbase.transactions.model.BackbaseTransaction;
import com.backbase.transactions.model.openbank.Transaction;

import java.util.ArrayList;
import java.util.List;

public class TransactionParser {
    public static List<BackbaseTransaction> fromOpenbankToBackbase(List<Transaction> transactions) {
        List<BackbaseTransaction> backbaseTransactions = new ArrayList<>();

        transactions.forEach(t -> {
            BackbaseTransaction backbaseTransaction = new BackbaseTransaction();
            backbaseTransaction.setId(t.getId());
            backbaseTransaction.setAccountId(t.getThisAccount().getId());
            backbaseTransaction.setCounterpartyAccount(t.getOtherAccount().getNumber());
            backbaseTransaction.setCounterpartyName(t.getOtherAccount().getHolder().getName());
            backbaseTransaction.setCounterPartyLogoPath(t.getOtherAccount().getMetadata().getImageUrl());
            backbaseTransaction.setInstructedAmount(t.getDetails().getValue().getAmount());
            backbaseTransaction.setInstructedCurrency(t.getDetails().getValue().getCurrency());
            backbaseTransaction.setTransactionAmount(t.getDetails().getValue().getAmount());
            backbaseTransaction.setTransactionCurrency(t.getDetails().getValue().getCurrency());
            backbaseTransaction.setTransactionType(t.getDetails().getType());
            backbaseTransaction.setDescription(t.getDetails().getDescription());

            backbaseTransactions.add(backbaseTransaction);
        });

        return backbaseTransactions;
    }
}
