package com.backbase.transactions.services;

import com.backbase.transactions.model.BackbaseTransaction;
import com.backbase.transactions.model.TotalAmount;
import com.backbase.transactions.model.openbank.Transaction;
import com.backbase.transactions.model.openbank.Transactions;
import com.backbase.transactions.parser.TransactionParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.*;

@Service
@PropertySource({"classpath:application.properties"})
public class TransactionsService {
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private Environment environment;

    private Transactions getTransactionsFromOpenBank() {
        return restTemplate.getForObject(environment.getProperty("openbank.endpoint"), Transactions.class);
    }

    public List<BackbaseTransaction> getAllTransactions() {
        Transactions transactions = getTransactionsFromOpenBank();
        return TransactionParser.fromOpenbankToBackbase(transactions.getTransactions());
    }

    public List<BackbaseTransaction> getTransactionByType(String transactionType) {
        Transactions transactions = getTransactionsFromOpenBank();
        List<Transaction> filteredTransactions = transactions.getTransactions()
                .stream()
                .filter(t -> transactionType.equals(t.getDetails().getType()))
                .collect(toList());
        return TransactionParser.fromOpenbankToBackbase(filteredTransactions);
    }

    public List<TotalAmount> getTotalAmountByType() {
        Transactions transactions = getTransactionsFromOpenBank();
        List<TotalAmount> totalAmounts = new ArrayList<>();

        //group by transaction type and then sum the amounts accordingly
        transactions.getTransactions()
                .stream()
                .filter(t -> t.getDetails().getType() != null) //Does nulls transactions types needs to be grouped?
                .collect(groupingBy(t -> t.getDetails().getType(),
                        summingDouble(t -> t.getDetails().getValue().getAmount())))
                .forEach((type, totalAmount) -> totalAmounts.add(new TotalAmount(type, totalAmount)));

        return totalAmounts;
    }
}
