package com.backbase.transactions.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity
public class ApiSecurityConfig  {
    @Bean
    public UserDetailsService userDetailsService() throws Exception {
        User.UserBuilder builder = User.withDefaultPasswordEncoder();
        UserDetails user = builder.username("user").password("password").roles("USER").build();
        UserDetails admin = builder.username("admin").password("password").roles("USER", "ADMIN").build();
        return new InMemoryUserDetailsManager(user, admin);
    }
}
