package com.backbase.transactions.model;

import java.io.Serializable;

public class TotalAmount implements Serializable {
    private String transactionType;
    private double totalAmount;

    public TotalAmount() {}

    public TotalAmount(String transactionType, double totalAmount) {
        this.transactionType = transactionType;
        this.totalAmount = totalAmount;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }
}
