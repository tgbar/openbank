package com.backbase.transactions.model.openbank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Account implements Serializable {
    private String id;
    private Holder holder;
    private String number;
    private AccountMetadata metadata;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Holder getHolder() {
        return holder;
    }

    public void setHolder(Holder holder) {
        this.holder = holder;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public AccountMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(AccountMetadata metadata) {
        this.metadata = metadata;
    }
}
