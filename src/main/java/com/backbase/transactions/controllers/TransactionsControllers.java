package com.backbase.transactions.controllers;

import com.backbase.transactions.model.BackbaseTransaction;
import com.backbase.transactions.model.TotalAmount;
import com.backbase.transactions.services.TransactionsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
@Api(value = "transactions", description = "Service that recovers openbank transactions")
public class TransactionsControllers {
    @Autowired
    private TransactionsService transactionsService;
    private static final Logger LOGGER = Logger.getLogger(TransactionsControllers.class.getName());

    @ApiOperation(value = "Returns all transactions", response = BackbaseTransaction[].class)
    @GetMapping("/transactions")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getAllTransactions() {
        return ResponseEntity.ok(transactionsService.getAllTransactions());
    }

    @ApiOperation(value = "Returns all transactions associated with the passed transaction type", response = BackbaseTransaction[].class)
    @GetMapping("/transaction")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getTransactionByType(@RequestParam String transactionType) {
        return ResponseEntity.ok(transactionsService.getTransactionByType(transactionType));
    }

    @ApiOperation(value = "Returns the amount sum grouped by transaction type", response = TotalAmount[].class)
    @GetMapping("/transactions/amount")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getTotalAmountByType() {
        return ResponseEntity.ok(transactionsService.getTotalAmountByType());
    }
}
