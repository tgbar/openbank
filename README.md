# To compile the project the following command should be executed:

	mvn clean install

# To run the project the following command should be executed (embedded jetty):
	mvn jetty:run

# API Endpoints

## http://localhost:8080/transactions

## http://localhost:8080/transaction?transactionType=<name>

## http://localhost:8080/transactions/amount

# Testing

	curl -v -i --user user:password <endpoint>


